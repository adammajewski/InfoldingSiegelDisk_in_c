
 
/*
 
  c console program
  It can be compiled and run under Linux, windows, Mac 
  It needs gcc
 
  --------------------------------------
  draws critical orbit for f(z)=z*z+c 
  
 
 
 
  ------------------------------------------
  one can change :
  
  - n 
  - iSide ( width of image = iXmax = (iSide) 
  - NrOfCrOrbitPoints = ; // check rang of type for big numbers : iMax, i, NrOfCrOrbitPoints


 %lld and %llu for print long long int
  -----------------------------------------
  1.pgm file code is  based on the code of Claudio Rocchini
  http://en.wikipedia.org/wiki/Image:Color_complex_plot.jpg
  create 8 bit color graphic file ,  portable gray map file = pgm 
  see http://en.wikipedia.org/wiki/Portable_pixmap
  to see the file use external application ( graphic viewer)
  I think that creating graphic can't be simpler
  ---------------------------
  2. first it creates data array which is used to store color values of pixels,
  fills tha array with data and after that writes the data from array to pgm file.
  It alows free ( non sequential) acces to "pixels"
  -------------------------------------------
  Here are 4 items : 
  1. complex plane Z with points z = zx+zy*i ( dynamic plane and world coordinate )
  2. virtual 2D array indexed by iX and iYmax ( integer or screen coordinate )
  3. memory 1D array data  indexed by i =f(iX,iY)
  4. pgm file 
 
 
 
  Adam Majewski   fraktal.republika.pl 
 
 
  to compile : 
  gcc d.c -lm -Wall -march=native
  to run ( Linux console) :
  time ./a.out



  convert -version
 
  convert data0.pgm -convolve "-1,-1,-1,-1,8,-1,-1,-1,-1"  -threshold 5% -negate data0.png
  convert data0.pgm -convolve "0,1,0,1,1,1,0,1,0" -threshold 5% -negate data0c.png
convert data0.pgm -convolve "-0.125,0,0.125,  -0.25,0,0.25,  -0.125,0,0.125" -threshold 5% -negate data0g.png

  convert data0.pgm -edge 3 -negate data0e.png
  convert data0.pgm -edge 3 -negate data0e.png
http://www.imagemagick.org/Usage/transform/#vision
"As you can see, the edge is added only to areas with a color gradient that is more than 50% white! I don't know if this is a bug or intentional, but it means that the edge in the above is located almost completely in the white parts of the original mask image. This fact can be extremely important when making use of the results of the "-edge" operator. For example if you are edge detecting an image containing an black outline, the "-edge" operator will 'twin' the black lines, producing a weird result." 

  convert data0.pgm -negate -edge 3 data0n.png
  convert data0n.png -edge 3 -negate data0e.png


http://unix.stackexchange.com/questions/299218/imagemagick-how-to-thicken-lines

convert 4.pgm -negate 4n.pgm
convert 4n.pgm -morphology Dilate Octagon 4nf.pgm
convert 4n.pgm -morphology Thicken '3x1+2+0:1,0,0' 4nfb.pgm
convert 4n.pgm -morphology Thicken ConvexHull 4nfc.pgm
convert 4nf.pgm -negate 4thick.pgm


--------------------
check if curve is closed : 
if x(1) == x(end) && y(1) == y(end)
  % It's closed
else
  % It's not closed
end


---------------------------------------------------------------------------------

http://www.scholarpedia.org/article/File:InfoldingSiegelDisk.gif
1,000 × 1,000 pixels, file size: 91 KB, MIME type: image/gif, looped, 9 frames, 12s)
The Siegel disks have been translated in the plane so that the critical point is always at the same point on the screen (near the top). 


for n :
 0,1   I have used  1.0e5 = pow(10,5) points 
 n= 2                    1.0e6 = pow(10,6)
 n = 3                   1.0e7 = pow(10,7)
 n = 4                   1.0e8 = pow(10,8) // good result
 n= 5                    1.0e9 = pow(10,9) // not good 
 
For n=5 I have to try pow(10,12).
 
Do you use such high number of iterations or other method ?

I think in this particular case, I iterated a lot. However, in some other pictures, I used an acceleration method, an approximation of a high iterate of f.



--------------------------------------------------------

(%i5) e0:bfloat(cfdisrep([0,1,2,4,4,c]));
(%o5)  6.90983385919269333377918690283855111536837789999636063622128b-1
(%i6) e1:bfloat(cfdisrep([0,1,2,4,4,10,c]));
(%o6)  6.90940653590858345205900333693231459583929903277216782489572b-1
(%i7) e2:bfloat(cfdisrep([0,1,2,4,4,10^2,c]));
(%o7)  6.90912381108070743953290526690429251279660195160237673633147b-1

(%i8) e3:bfloat(cfdisrep([0,1,2,4,4,10^3,c]));
(%o8)  6.90909421331077674912831355964859580252020147075327146840293b-1

(%i9) e4:bfloat(cfdisrep([0,1,2,4,4,10^4,c]));
(%o9)  6.90909123965376225147306218871548821073377014383362752163859b-1

(%i10) e5:bfloat(cfdisrep([0,1,2,4,4,10^5,c]));
(%o10) 6.90909094214860373154103745626419162194365585020091374566415b-1

(%i11) e6:bfloat(cfdisrep([0,1,2,4,4,10^6,c]));
(%o11) 6.90909091239669264887898182284536032143262445829248943506691b-1

(%i12) e7:bfloat(cfdisrep([0,1,2,4,4,10^7,c]));
(%o12) 6.90909090942148758764580793509997058314465248663422318011972b-1

(%i13) e8:bfloat(cfdisrep([0,1,2,4,4,10^8,c]));
(%o13) 6.90909090912396694199216055201332263713180254323741379104146b-1

(%i14) e9:bfloat(cfdisrep([0,1,2,4,4,10^9,c]));
(%o14)  6.9090909090942148760314918534473410035349593667510644807155b-1

(%i15) e10:bfloat(cfdisrep([0,1,2,4,4,10^10,c]));
(%o15) 6.90909090909123966942147194332785516326702737819678667743891b-1















*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <complex.h>
#include <string.h>


#define NMAX 10
#define L  (NMAX +1)


 
 
/* iXmax/iYmax =  */
const int iSide = 1000;
int iXmax ; /* width of image in pixels = (15*iSide); */
int iYmax ;
int iLength ;

/* dynamic 1D arrays for colors ( shades of gray )  */
  
  unsigned char *data;



/* */
const double ZxMin = -0.7;
const double ZxMax = 0.3;
const double ZyMin = -0.9;
const double ZyMax = 0.1;
/* (ZxMax-ZxMin)/(ZyMax-ZyMin)= iXmax/iYmax  */
 
 
double PixelWidth ;
double PixelHeight ;
 
 unsigned int period=1;
 // unsigned int m;
 // check rang of type for big numbers : iMax, i, NrOfCrOrbitPoints
  unsigned long long int NrOfCrOrbitPoints ;
 
 
 
 double radius = 1.0;
 double tt[L] = { 	0.690983385919269333377918690283855111536837789999636063622128,
		  	0.690940653590858345205900333693231459583929903277216782489572,
 			0.690912381108070743953290526690429251279660195160237673633147,
 			0.690909421331077674912831355964859580252020147075327146840293,
			0.690909094214860373154103745626419162194365585020091374566415,
                        0.690909091239669264887898182284536032143262445829248943506691,
                        0.690909090942148758764580793509997058314465248663422318011972,
                        0.690909090912396694199216055201332263713180254323741379104146,
                        0.69090909090942148760314918534473410035349593667510644807155,
                        0.690909090909123966942147194332785516326702737819678667743891                    
                      	}; 
/* fc(z) = z*z + c */
/*   */
complex double C; 
double Cx; /* C = Cx + Cy*i   */
double Cy ;
 
 
 
/* colors */
const unsigned int MaxColorComponentValue=255; /* color component is coded from 0 to 255 ;  it is 8 bit color file */
const int iExterior = 245; /* exterior of Julia set */
const int iJulia = 0; /* border , boundary*/
const int iInterior = 230;
 
 
 
/* ----------------------- functions ---------------------------------------- */


/* find c in component of Mandelbrot set 
 
   uses code by Wolf Jung from program Mandel
   see function mndlbrot::bifurcate from mandelbrot.cpp
   http://www.mndynamics.com/indexp.html

*/
double complex GiveC(double InternalAngleInTurns, double InternalRadius, unsigned int Period)
{
  //0 <= InternalRay<= 1
  //0 <= InternalAngleInTurns <=1
  double t = InternalAngleInTurns *2*M_PI; // from turns to radians
  double R2 = InternalRadius * InternalRadius;
  //double Cx, Cy; /* C = Cx+Cy*i */
  switch ( Period ) // of component 
    {
    case 1: // main cardioid
      Cx = (cos(t)*InternalRadius)/2-(cos(2*t)*R2)/4; 
      Cy = (sin(t)*InternalRadius)/2-(sin(2*t)*R2)/4; 
      break;
    case 2: // only one component 
      Cx = InternalRadius * 0.25*cos(t) - 1.0;
      Cy = InternalRadius * 0.25*sin(t); 
      break;
      // for each iPeriodChild  there are 2^(iPeriodChild-1) roots. 
    default: // higher periods : to do, use newton method 
      Cx = 0.0;
      Cy = 0.0; 
      break; }

  return Cx + Cy*I;
}




 
 
unsigned int f(unsigned int iX, unsigned int iY)
/* 
   gives position of point (iX,iY) in 1D array  ; uses also global variables 
   it does not check if index is good  so memory error is possible 
*/
{return (iX + iY*iXmax );}
 
 
 
int DrawPoint( double Zx, double Zy, unsigned char data[])
{
  unsigned int iX,iY; /* indices of 2D virtual array (image) = integer coordinate */
  unsigned int index; /* index of 1D array  */ 
 
  //if ( Zx < ZxMin || ZxMax  < Zx || Zy < ZyMin || ZyMax < Zy ) {  printf("   point z = %f , %f out of bounds  \n",Zx, Zy); return 1; } 
  
  iX = (int)((Zx-ZxMin)/PixelWidth);
  iY = (int)((ZyMax-Zy)/PixelHeight); // reverse Y axis
  index = f(iX,iY);
  
  
  data[index] = iJulia;  /* draw */
  return 0;
}
 

 
 
/*

 check rang of type for big numbers : iMax, i, NrOfCrOrbitPoints
x := x^2 - y^2 + cx
y := 2 x y + cy
*/


int DrawCriticalOrbit(unsigned long long int iMax,  unsigned char A[] )
{
  unsigned long long int i; /* nr of point of critical orbit */
  double Zx,Zy, tmp;
   
 
  /* critical point z = 0 */
  Zx = 0.0;
  Zy = 0.0;
  DrawPoint(Zx,Zy,A);
  
  /* forward orbit of critical point  */
  for (i=1;i<=iMax ;i++)
    {
      /* f(z) = z*z+c */
      tmp = 2*Zx*Zy + Cy;
      Zx = Zx*Zx - Zy*Zy + Cx;
      Zy = tmp;
       
 
      
     // if (Zx2+Zy2>4) { printf("   point z = %f , %f escapes \n",Zx, Zy); break;}
      
      DrawPoint(Zx,Zy,A); /* draws critical orbit */
      printf("  %f \r",(double) i/iMax); /* progres info */        

    }

    
   
  return 0;
 
}


/* 
close the curve by filing gaps by streight lines
curve is the simple closed 2d plane curve 

*/
int CloseTheCurve( unsigned char A[] ){

return 0;
}


int ClearArray( unsigned char A[] )
{
  unsigned int index; /* index of 1D array  */
  for(index=0;index<iLength-1;++index) 
                A[index]=iExterior;
  return 0;
}


// Check Orientation of image : first quadrant in upper right position
// uses global var :  ...
int CheckOrientation(unsigned char A[] )
{
  unsigned int ix, iy; // pixel coordinate 
  double Zx, Zy; //  Z= Zx+ZY*i;
  unsigned i; /* index of 1D array */
  for(iy=0;iy<=iYmax;++iy) 
    {
      Zy =ZyMax - iy*PixelHeight;
      for(ix=0;ix<=iXmax;++ix) 
	{

	  // from screen to world coordinate 
	  Zx = ZxMin + ix*PixelWidth ;
	  i = f(ix, iy); /* compute index of 1D array from indices of 2D array */
	  if (Zx>0 && Zy>0) A[i] = 255-A[i];   // check the orientation of Z-plane by marking first quadrant */

	}
    }
   
  return 0;
}





int SaveArray2pgm(unsigned char A[], unsigned int n)
{

FILE * fp;
  char name [20]; /* name of file */
  sprintf(name,"%u",n); /*  */
  char *filename =strcat(name,".pgm");
  char *comment="# C= ";/* comment should start with # */
  /* save image to the pgm file  */      
  fp= fopen(filename,"wb"); /*create new file,give it a name and open it in binary mode  */
  fprintf(fp,"P5\n %s\n %u %u\n %u\n",comment,iXmax,iYmax,MaxColorComponentValue);  /*write header to the file*/
  fwrite(A,iLength,1,fp);  /*write image data bytes to the file in one step */
  printf("File %s saved. \n", filename);
  fclose(fp);
  return 0;
}
 



unsigned long long int GiveNrOfCrOrbitPoints( int n){

  unsigned long long int i ; 
  switch( n )
  { case 0 : i = pow(10,5); break;
    case 1 : i = pow(10,5); break;  
    case 2 : i = pow(10,6); break;
    case 3 : i = pow(10,7); break;
    case 4 : i = pow(10,8); break;
    case 5 : i = pow(10,9); break;
    default: i = pow(10,5); break;
  }
  return i;
}

int setup(int n){

  

/* unsigned int iX,iY;  indices of 2D virtual array (image) = integer coordinate */
  iXmax = iSide-1; /* height of image in pixels */
  iYmax = iSide-1;
  iLength = (iSide*iSide);
 
 //
  PixelWidth =  ((ZxMax-ZxMin)/iXmax);
  PixelHeight = ((ZyMax-ZyMin)/iYmax);
 
 
  //
  data = malloc( iLength * sizeof(unsigned char) );
  if (data == NULL )
    {
      fprintf(stderr," Could not allocate memory");
      return 1;
    }
  
          
  ClearArray( data );
  
  //
  C = GiveC(tt[n], radius, period);
  Cx = creal(C); 
  Cy = cimag(C); 
  // 
  NrOfCrOrbitPoints = pow(10,5+n);
 

return 0;
};


 int info(int n){

  printf("   n = %d  \n", n);
  printf("   t = %.16f \n", tt[n]);
  printf("   c = %.16f , %.16f  \n",Cx, Cy);
  printf("   NrOfCrOrbitPoints = %.llu  \n",NrOfCrOrbitPoints);
  return 0;
};
 






/* --------------------------------------------------------------------------------------------------------- */
 
int main(){
  
int n;   
 
  for (n=0; n<=NMAX; ++n){ 

   setup(n);
  
  /* ------------------ draw ----------------------- */
  printf(" Draw %llu points of critical orbit to array \n",NrOfCrOrbitPoints);       
  DrawCriticalOrbit(NrOfCrOrbitPoints, data);
  printf(" save  data array to the pgm file \n");
  SaveArray2pgm(data, n);

  //CheckOrientation(data);
  //SaveArray2pgm(data, n+1000);
   
  info(n); 

 }
  
  free(data);
  
 
 
 
 
 
  return 0;
}


